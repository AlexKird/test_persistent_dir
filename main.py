from fastapi import FastAPI
from starlette.responses import FileResponse
import os

app = FastAPI()


@app.post("/create-file/")
async def create_file(number: int):
    file_path = "/storage/number.txt"
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, "w") as file:
        file.write(str(number))
    return {"number": number}


@app.get("/read-file/")
async def read_file():
    file_path = "/storage/number.txt"
    if os.path.exists(file_path):
        with open(file_path, "r") as file:
            number = file.read()
        return {"number": int(number)}
    else:
        return {"number": 0}
